# eventsourced PostgreSQL event store

[![GoReportCard](https://goreportcard.com/badge/gitlab.com/unboundsoftware/eventsourced/pg)](https://goreportcard.com/report/gitlab.com/unboundsoftware/eventsourced/pg) [![GoDoc](https://godoc.org/gitlab.com/unboundsoftware/eventsourced/pg?status.svg)](https://godoc.org/gitlab.com/unboundsoftware/eventsourced/pg) [![Build Status](https://gitlab.com/unboundsoftware/eventsourced/pg/badges/main/pipeline.svg)](https://gitlab.com/unboundsoftware/eventsourced/pg/commits/main)[![coverage report](https://gitlab.com/unboundsoftware/eventsourced/pg/badges/main/coverage.svg)](https://gitlab.com/unboundsoftware/eventsourced/pg/commits/main)

Package `pg` provides a [PostgreSQL](https://postgresql.org) implementation of the event store which can be used with the [eventsourced framework](https://gitlab.com/unboundsoftware/eventsourced/eventsourced).

Download:
```shell
go get gitlab.com/unboundsoftware/eventsourced/pg
```



