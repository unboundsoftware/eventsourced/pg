# Changelog

All notable changes to this project will be documented in this file.

## [1.16.0] - 2025-02-27

### 🚀 Features

- *(linter)* Enable parallel runners and set timeout for checks

### 🐛 Bug Fixes

- *(deps)* Update module github.com/pressly/goose/v3 to v3.23.1
- *(deps)* Update module github.com/pressly/goose/v3 to v3.24.0
- *(deps)* Update module github.com/sanity-io/litter to v1.5.6
- *(deps)* Update module github.com/pressly/goose/v3 to v3.24.1
- *(deps)* Update module github.com/sanity-io/litter to v1.5.7
- *(ci)* Update golangci-lint to version 1.64.4
- *(deps)* Update module github.com/sanity-io/litter to v1.5.8
- Update Go image to use amd64 architecture 

### 🚜 Refactor

- Update event handling to use ExternalEvent type

## [1.15.0] - 2024-11-27

### 🚀 Features

- Add external events handling functionality

### 🐛 Bug Fixes

- *(deps)* Update module github.com/pressly/goose/v3 to v3.23.0
- *(deps)* Update module github.com/stretchr/testify to v1.10.0
- *(deps)* Update module gitlab.com/unboundsoftware/eventsourced/eventsourced to v1.16.0

## [1.14.4] - 2024-10-06

### 🐛 Bug Fixes

- *(deps)* Update module github.com/pressly/goose/v3 to v3.20.0
- *(deps)* Update module github.com/jmoiron/sqlx to v1.4.0
- *(deps)* Update module github.com/pressly/goose/v3 to v3.21.0
- *(deps)* Update module github.com/pressly/goose/v3 to v3.21.1
- *(deps)* Update module github.com/pressly/goose/v3 to v3.22.0
- *(deps)* Update module github.com/pressly/goose/v3 to v3.22.1
- *(deps)* Update module gitlab.com/unboundsoftware/eventsourced/eventsourced to v1.15.0

### ⚙️ Miscellaneous Tasks

- Add release flow

## [1.14.3] - 2024-03-20

### 🚀 Features

- Also handle non-tagged fields and ignore unexported

## [1.14.2] - 2024-03-20

### 🐛 Bug Fixes

- Collect all errors before returning

## [1.14.1] - 2024-03-20

### 🚀 Features

- Check json tag names for duplicates

## [1.14.0] - 2024-03-18

### 🚀 Features

- Convert payload to JSONB for performance

### 🐛 Bug Fixes

- *(deps)* Update module github.com/stretchr/testify to v1.9.0
- *(deps)* Update module github.com/pressly/goose/v3 to v3.19.1
- *(deps)* Update module github.com/pressly/goose/v3 to v3.19.2

## [1.13.7] - 2023-12-17

### 🐛 Bug Fixes

- Handle empty result set
- Correct trace name

## [1.13.6] - 2023-12-14

### 🚀 Features

- Chunk rows to reduce DB temp-storage if many events

### 🐛 Bug Fixes

- Add correct args

### ⚙️ Miscellaneous Tasks

- Update version of Go and golangci-lint

## [1.13.5] - 2023-11-23

### ⚙️ Miscellaneous Tasks

- Retract v1.13.4

## [1.13.4] - 2023-11-23

### 🚀 Features

- Replace unique index with deferrable constraint

## [1.13.3] - 2023-11-16

### 🚀 Features

- Add slog for Goose logging
- Migrate to Goose provider

### ⚙️ Miscellaneous Tasks

- Call ExpectationsWereMet on sqlmock

## [1.13.2] - 2023-11-09

### 🐛 Bug Fixes

- Add global sequence number to event wrapper

### ⚙️ Miscellaneous Tasks

- Use go 1.21.4

## [1.13.1] - 2023-10-27

### ⚙️ Miscellaneous Tasks

- Remove go patch level

## [1.13.0] - 2023-10-26

### 🚀 Features

- Move migrations to Goose
- Handle EventGlobalSequence

### ⚙️ Miscellaneous Tasks

- Update Go and golangci-lint versions
- Remove deprecated GetSnapshot

## [1.12.0] - 2023-07-07

### 🐛 Bug Fixes

- Remove columns from unique events index

### ⚙️ Miscellaneous Tasks

- *(deps)* Update to Go 1.20.5

## [1.11.2] - 2023-04-20

### 🐛 Bug Fixes

- Order events by sequence_no rather than id

### ⚙️ Miscellaneous Tasks

- Change default branch to main
- Switch to manual rebases for Dependabot
- Update to golang 1.20.1
- Update to golangci-lint 1.51.2
- Update to Go 1.20.3

## [1.11.1] - 2023-01-26

### 🚀 Features

- Add RestoreSnapshot in preparation for Store API change

## [1.11.0] - 2023-01-24

### 🚀 Features

- Update to new Store API

## [1.10.3] - 2022-11-21

### ⚙️ Miscellaneous Tasks

- Add more info to trace name

## [1.10.2] - 2022-11-15

### ⚙️ Miscellaneous Tasks

- Add context to allow span nesting

## [1.10.1] - 2022-11-11

### 🚀 Features

- Add tracing

## [1.10.0] - 2022-10-11

### 🚀 Features

- [**breaking**] Add context to fulfill eventsourced contract

### 🐛 Bug Fixes

- Lint errors
- Run builds with Go 1.19.2 to fix vulnerabilities

### ⚙️ Miscellaneous Tasks

- Replace golint with golangci-lint and run as separate step
- Add vulnerability-check

## [1.9.0] - 2022-05-23

### 🚀 Features

- Implement new Eventstore interface for skipping undefined events when replaying
- Ouput warning when no types are registered

### ⚙️ Miscellaneous Tasks

- Fix typo in error message

## [1.8.0] - 2022-05-20

### 🐛 Bug Fixes

- Update test-aggregates to implement new interface method

### ⚙️ Miscellaneous Tasks

- Add dependabot config
- Remove dependabot-standalone
- Change to codecov binary instead of bash uploader
- Make sure event types are pointers when setting up store

## [1.7.3] - 2021-01-25

### 🐛 Bug Fixes

- Only drop event_ix if aggregate_name is not part of it

## [1.7.2] - 2020-12-08

### 🐛 Bug Fixes

- Add aggregate_name to max sequence no query

## [1.7.1] - 2020-12-08

### 🐛 Bug Fixes

- Use transaction when inserting aggregate

## [1.7.0] - 2020-12-04

### 🐛 Bug Fixes

- Handle aggregate existance

## [1.6.1] - 2020-12-03

### 🐛 Bug Fixes

- Make sure we return ErrAggregateAlreadyExists if insertion fails

## [1.6.0] - 2020-12-03

### 🐛 Bug Fixes

- Move aggregate creation to store event transaction

## [1.5.4] - 2020-11-25

### 🚀 Features

- *(eventsfrom)* Implement new method in interface for Store

### 🐛 Bug Fixes

- Use UTC to fix failing test

### ⚙️ Miscellaneous Tasks

- Change to use fromID instead of sequenceNo since sequenceNo is per aggregate

## [1.5.3] - 2020-11-18

### 🐛 Bug Fixes

- Type assert pointer

## [1.5.2] - 2020-11-18

### 🐛 Bug Fixes

- Revert back to pq again

## [1.5.1] - 2020-11-18

### 🚀 Features

- Return ErrAggregateAlreadyExists if aggregate exists

## [1.5.0] - 2020-11-03

### 🚀 Features

- [**breaking**] Handle different aggregates with same id

### ⚙️ Miscellaneous Tasks

- Add codecov upload
- Simplify pipeline
- Update year in copyright
- Go mod tidy

## [1.4.0] - 2020-03-24

### 🐛 Bug Fixes

- Unmarshal snapshot to aggregate instead

## [1.3.0] - 2019-12-19

### 🚀 Features

- Aggregate roots. BREAKING change since I've renamed an index

### ⚙️ Miscellaneous Tasks

- Update eventsourced-version

## [1.2.2] - 2019-11-19

### 🐛 Bug Fixes

- Add tag for sequence_no column

## [1.2.1] - 2019-11-19

### 🚀 Features

- Make GetEvents return a wrapper which contains the sequence no as well as the event

## [1.2.0] - 2019-11-19

### 🚀 Features

- Use new version of eventsourced

## [1.1.2] - 2019-11-06

### 🐛 Bug Fixes

- Remove payload from indexes

## [1.1.1] - 2019-11-05

### 💼 Other

- Close row to avoid hanging connections

<!-- generated by git-cliff -->
