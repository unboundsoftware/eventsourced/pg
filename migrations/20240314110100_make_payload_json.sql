-- +goose Up
alter table events
    alter column payload TYPE jsonb using payload::jsonb;

alter table snapshots
    alter column payload TYPE jsonb using payload::jsonb;

-- +goose Down
alter table events
    alter column payload TYPE text;

alter table snapshots
    alter column payload TYPE text;
