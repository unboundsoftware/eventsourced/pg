-- +goose Up
create table if not exists aggregates
(
    id   text not null,
    name text not null,
    primary key (id, name)
);

create unique index if not exists aggregate_ix
    on aggregates (id, name);

create table if not exists events
(
    id             bigserial
        primary key,
    name           text                     not null,
    aggregate_id   text                     not null,
    sequence_no    bigint                   not null,
    payload        text                     not null,
    tstamp         timestamp with time zone not null,
    aggregate_name text                     not null
);

create unique index if not exists event_ix
    on events (aggregate_id, aggregate_name, sequence_no);

create table if not exists snapshots
(
    id           bigserial
        primary key,
    name         text                     not null,
    aggregate_id text                     not null,
    sequence_no  bigint                   not null,
    payload      text                     not null,
    tstamp       timestamp with time zone not null
);

create unique index if not exists snapshot_ix
    on snapshots (aggregate_id, sequence_no, id, name, tstamp);

-- +goose Down
drop table aggregates;
drop table events;
drop table snapshots;
