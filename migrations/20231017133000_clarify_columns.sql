-- +goose Up
alter table events
    rename column id to global_sequence_no;
alter table events
    rename column sequence_no to aggregate_sequence_no;

-- +goose Down
alter table events
    rename column global_sequence_no to id;
alter table events
    rename column aggregate_sequence_no to sequence_no;
