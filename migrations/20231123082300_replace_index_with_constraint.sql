-- +goose Up
drop index event_ix;

alter table events
add constraint events_ix
unique (aggregate_id, aggregate_name, aggregate_sequence_no)
deferrable initially IMMEDIATE;

-- +goose Down
alter table events
drop constraint events_ix;

create unique index if not exists event_ix
    on events (aggregate_id, aggregate_name, aggregate_sequence_no);
