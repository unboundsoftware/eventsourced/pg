// Copyright (c) 2020 Unbound Software Development Svenska AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package pg

import (
	"bytes"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log/slog"
	"math"
	"reflect"
	"sync"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"github.com/sanity-io/litter"
	"github.com/stretchr/testify/assert"

	"gitlab.com/unboundsoftware/eventsourced/eventsourced"
)

var tstamp, _ = time.Parse("2006-01-02 15:04:05.999999999", "2019-08-07 10:32:44.123456789")

func TestStore_Migrations_Error(t *testing.T) {
	db, _, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	_, err = New(context.Background(), db, WithEventTypes(&Area{}))
	assert.EqualError(t, err, "failed to initialize: failed to check if version table exists: failed to check if table exists: all expectations were already fulfilled, call to Query 'SELECT EXISTS ( SELECT 1 FROM pg_tables WHERE (current_schema() IS NULL OR schemaname = current_schema()) AND tablename = 'goose_db_version_pg' )' with args [] was not expected")
}

func TestStore_Migrations(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	_, err = New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_GetEvents_ErrorFetching(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)
	mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
		WithArgs("123", "pg.Asset", 0).
		WillReturnError(errors.New("attribute aggregate_id does not exist in table events"))

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	events, err := store.GetEvents(context.Background(), &Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}}, nil)
	assert.EqualError(t, err, "attribute aggregate_id does not exist in table events")
	assert.Equal(t, 0, len(events))
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_GetEvents_UnknownEventType(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
		WithArgs("123", "pg.Asset", 0).
		WillReturnRows(
			mock.NewRows([]string{"global_sequence_no", "name", "payload", "tstamp"}).
				AddRow(1, "pg.Unknown", []byte(`{}`), tstamp))

	_, err = store.GetEvents(context.Background(), &Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}}, nil)
	assert.EqualError(t, err, "no event type <pg.Unknown> found in registered event types [pg.Area]")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_GetEvents_NonPointerType(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	_, err = store.GetEvents(context.Background(), AssetNonPointer{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}}, nil)
	assert.EqualError(t, err, "GetEvents requires a pointer to an aggregate")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_New_NoDbProvided(t *testing.T) {
	_, err := New(context.Background(), nil, WithEventTypes(&AssetRemoved{}))
	assert.ErrorContains(t, err, "db must not be nil")
}

func TestStore_New_NonPointerEventType(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	_, err = New(context.Background(), db, WithEventTypes(AssetRemoved{}))
	assert.ErrorContains(t, err, "event type pg.AssetRemoved must be pointer")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_New_EventTypeNotImplementingEvent(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	_, err = New(context.Background(), db, WithEventTypes(&Asset{}))
	assert.ErrorContains(t, err, "event type pg.Asset does not implement eventsourced.Event")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_GetEvents_ErrorUnmarshal(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
		WithArgs("123", "pg.Asset", 0).
		WillReturnRows(
			mock.NewRows([]string{"global_sequence_no", "name", "payload", "tstamp"}).
				AddRow(1, "pg.Area", []byte("{"), tstamp))
	events, err := store.GetEvents(context.Background(), &Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}}, nil)
	assert.ErrorContains(t, err, "unexpected end of JSON input")
	assert.Equal(t, 0, len(events))
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_GetEvents_NoEvents(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
		WithArgs("123", "pg.Asset", 0).
		WillReturnRows(
			mock.NewRows([]string{"global_sequence_no", "name", "payload", "tstamp"}))

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	events, err := store.GetEvents(context.Background(), &Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}}, nil)
	assert.NoError(t, err)
	assert.Equal(t, 0, len(events))
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_GetEvents_OneEvent(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
		WithArgs("123", "pg.Asset", 0).
		WillReturnRows(
			mock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
				AddRow(1, 1, "pg.Area", []byte(`{}`), tstamp))

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	events, err := store.GetEvents(context.Background(), &Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}}, nil)
	assert.NoError(t, err)
	assert.Equal(t, 1, len(events))
	assert.Equal(t, 1, events[0].Event.GlobalSequenceNo())
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_GetEvents_ExternalSource(t *testing.T) {
	tests := []struct {
		name    string
		options []Option
		setup   func(mock sqlmock.Sqlmock)
		want    []eventsourced.EventWrapper
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "payload unmarshallable",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
					WithArgs("123", "pg.Asset", 0).
					WillReturnRows(
						mock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
							AddRow(1, 1, "eventsourced.ExternalEvent[*gitlab.com/unboundsoftware/eventsourced/pg.Area]", []byte(`{"TypeName:"pg.Area","Source":"external-system","Event":{}}`), tstamp))
			},
			want: nil,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "invalid character 'p' after object key")
			},
		},
		{
			name: "external event unmarshallable",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
					WithArgs("123", "pg.Asset", 0).
					WillReturnRows(
						mock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
							AddRow(1, 1, "eventsourced.ExternalEvent[*gitlab.com/unboundsoftware/eventsourced/pg.Area]", []byte(`{"WrappedTypeName":"pg.Area"\","Source":"external-system","Event":{}}`), tstamp))
			},
			want: nil,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "invalid character '\\\\' after object key:value pair")
			},
		},
		{
			name: "external event payload unmarshallable",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
					WithArgs("123", "pg.Asset", 0).
					WillReturnRows(
						mock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
							AddRow(1, 1, "eventsourced.ExternalEvent[*gitlab.com/unboundsoftware/eventsourced/pg.Area]", []byte(`{"WrappedTypeName":"pg.Area"\","Source":"external-system","Event":{}}`), tstamp))
			},
			want: nil,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "invalid character '\\\\' after object key:value pair")
			},
		},
		{
			name: "external event type undefined",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
					WithArgs("123", "pg.Asset", 0).
					WillReturnRows(
						mock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
							AddRow(1, 1, "eventsourced.ExternalEvent[*gitlab.com/unboundsoftware/eventsourced/pg.Undefined]", []byte(`{"WrappedTypeName":"pg.Undefined","Source":"external-system","Event":{}}`), tstamp))
			},
			want: nil,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "no event type <eventsourced.ExternalEvent[*gitlab.com/unboundsoftware/eventsourced/pg.Undefined]> found in registered event types [eventsourced.ExternalEvent[*gitlab.com/unboundsoftware/eventsourced/pg.Area] pg.Area]")
			},
		},
		{
			name:    "external event type skip undefined",
			options: []Option{WithIgnoreUndefinedEventTypesDuringReplay()},
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
					WithArgs("123", "pg.Asset", 0).
					WillReturnRows(
						mock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
							AddRow(1, 1, "eventsourced.ExternalEvent", []byte(`{"TypeName":"pg.Undefined","Source":"external-system","Event":{}}`), tstamp))
			},
			want:    nil,
			wantErr: assert.NoError,
		},
		{
			name: "event payload unmarshallable",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
					WithArgs("123", "pg.Asset", 0).
					WillReturnRows(
						mock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
							AddRow(1, 1, "eventsourced.ExternalEvent[*gitlab.com/unboundsoftware/eventsourced/pg.Area]", []byte(`{"TypeName":"pg.Area","Source":"external-system","wrappedEvent":{"name":1}}`), tstamp))
			},
			want: nil,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "json: cannot unmarshal number into Go struct field Area.wrappedEvent.Name of type string")
			},
		},
		{
			name: "payload event unmarshallable",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
					WithArgs("123", "pg.Asset", 0).
					WillReturnRows(
						mock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
							AddRow(1, 1, "eventsourced.ExternalEvent[*gitlab.com/unboundsoftware/eventsourced/pg.Area]", []byte(`{"typeName":"pg.Area","Source":"external-system","wrappedEvent":{}}`), tstamp))
			},
			want: []eventsourced.EventWrapper{
				{
					Event: &eventsourced.ExternalEvent[*Area]{
						BaseEvent: eventsourced.BaseEvent{
							EventGlobalSequence: eventsourced.EventGlobalSequence{SeqNo: 1},
						},
						Source:       "external-system",
						TypeName:     "pg.Area",
						WrappedEvent: &Area{},
					},
					AggregateSequenceNo: 1,
				},
			},
			wantErr: assert.NoError,
		},
		{
			name: "success",
			setup: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
					WithArgs("123", "pg.Asset", 0).
					WillReturnRows(
						mock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
							AddRow(1, 1, "eventsourced.ExternalEvent[*gitlab.com/unboundsoftware/eventsourced/pg.Area]", []byte(`{"typeName":"pg.Area","Source":"external-system","wrappedEvent":{}}`), tstamp))
			},
			want: []eventsourced.EventWrapper{
				{
					Event: &eventsourced.ExternalEvent[*Area]{
						BaseEvent: eventsourced.BaseEvent{
							EventGlobalSequence: eventsourced.EventGlobalSequence{SeqNo: 1},
						},
						Source:       "external-system",
						TypeName:     "pg.Area",
						WrappedEvent: &Area{},
					},
					AggregateSequenceNo: 1,
				},
			},
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			assert.NoError(t, err)

			MigrationExpectations(mock)

			if tt.setup != nil {
				tt.setup(mock)
			}

			options := []Option{WithEventTypes(&Area{}, &eventsourced.ExternalEvent[*Area]{})}
			options = append(options, tt.options...)
			store, err := New(context.Background(), db, options...)
			assert.NoError(t, err)

			got, err := store.GetEvents(context.Background(), &Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}}, nil)
			if !tt.wantErr(t, err, fmt.Sprintf("GetEvents(%v, %v)", db, store)) {
				return
			}
			assert.Equal(t, tt.want, got, "GetEvents(%v, %v)", db, store)
			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}
}

func TestStore_GetEvents_SkipUndefined(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
		WithArgs("123", "pg.Asset", 0).
		WillReturnRows(
			mock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
				AddRow(1, 1, "pg.AssetCreated", []byte(`{}`), tstamp))

	store, err := New(context.Background(), db, WithEventTypes(&AssetRemoved{}), WithIgnoreUndefinedEventTypesDuringReplay())
	assert.NoError(t, err)

	events, err := store.GetEvents(context.Background(), &Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}}, nil)
	assert.NoError(t, err)
	assert.Equal(t, 0, len(events))
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_GetEvents_SkipUndefinedFails(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
		WithArgs("123", "pg.Asset", 0).
		WillReturnRows(
			mock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
				AddRow(1, 1, "pg.AssetCreated", []byte(`{}`), tstamp))

	store, err := New(context.Background(), db, WithEventTypes(&AssetRemoved{}))
	assert.NoError(t, err)

	_, err = store.GetEvents(context.Background(), &Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}}, nil)
	assert.EqualError(t, err, "no event type <pg.AssetCreated> found in registered event types [pg.AssetRemoved]")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_GetEvents_SinceSnapshot(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no").
		WithArgs("123", "pg.Asset", 7).
		WillReturnRows(
			mock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
				AddRow(1, 1, "pg.Area", []byte(`{}`), tstamp))

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	events, err := store.GetEvents(context.Background(), &Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}}, &snapshot{
		sequenceNo: 7,
		aggregate:  nil,
		when:       tstamp,
	})
	assert.NoError(t, err)
	assert.Equal(t, 1, len(events))
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_Events_SkipUndefined(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where global_sequence_no > $1 order by global_sequence_no limit 10000").
		WithArgs(0).
		WillReturnRows(
			mock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
				AddRow(1, 1, "pg.AssetCreated", []byte(`{}`), tstamp).
				AddRow(2, 2, "pg.AssetRemoved", []byte(`{}`), tstamp))
	mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where global_sequence_no > $1 order by global_sequence_no limit 10000").
		WithArgs(2).
		WillReturnRows(mock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}))

	store, err := New(context.Background(), db, WithEventTypes(&AssetRemoved{}), WithIgnoreUndefinedEventTypesDuringReplay())
	assert.NoError(t, err)

	var events []eventsourced.Event
	c := make(chan eventsourced.Event, 10)
	err = store.Events(context.Background(), 0, c)

	for evt := range c {
		events = append(events, evt)
	}
	assert.NoError(t, err)

	assert.Equal(t, 1, len(events))
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_Events_SkipUndefinedFails(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where global_sequence_no > $1 order by global_sequence_no limit 10000").
		WithArgs(0).
		WillReturnRows(
			mock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
				AddRow(1, 1, "pg.AssetCreated", []byte(`{}`), tstamp))

	store, err := New(context.Background(), db, WithEventTypes(&AssetRemoved{}))
	assert.NoError(t, err)

	c := make(chan eventsourced.Event)
	err = store.Events(context.Background(), 0, c)
	assert.EqualError(t, err, "no event type <pg.AssetCreated> found in registered event types [pg.AssetRemoved]")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreEvent_NonPointer_Aggregate(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	area := &Area{
		EventAggregateId: eventsourced.NewEventAggregateId("123"),
		Name:             "Test",
	}

	err = store.StoreEvent(
		context.Background(),
		AssetNonPointer{BaseAggregate: BaseAggregate{ID: eventsourced.Id(
			"123")}},
		area,
		1,
		false,
	)
	assert.EqualError(t, err, "StoreEvent requires a pointer to an aggregate")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreEvent_NonPointer_Event(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	event := NonPointerEvent("")

	err = store.StoreEvent(
		context.Background(),
		&Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}},
		event,
		1,
		false,
	)
	assert.EqualError(t, err, "StoreEvent requires a pointer to an event")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreEvent_ExternalContainer_NonPointer(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	store, err := New(context.Background(), db, WithEventTypes(&Area{}, &eventsourced.ExternalEvent[eventsourced.Event]{}))
	assert.NoError(t, err)

	event := &eventsourced.ExternalEvent[eventsourced.Event]{
		Source:       "external-service",
		TypeName:     "",
		WrappedEvent: NonPointerEvent(""),
	}

	err = store.StoreEvent(
		context.Background(),
		&Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}},
		event,
		1,
		false,
	)
	assert.EqualError(t, err, "StoreEvent requires a pointer to wrapped events")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreEvent_ExternalContainer(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectBegin()
	mock.ExpectQuery("insert into events (name, aggregate_id, aggregate_name, aggregate_sequence_no, payload, tstamp) values ($1, $2, $3, $4, $5, $6) returning global_sequence_no").
		WithArgs("eventsourced.ExternalEvent[*gitlab.com/unboundsoftware/eventsourced/pg.Area]", "external", "pg.Asset", 1, `{"id":"external","time":"0001-01-01T00:00:00Z","sequenceNo":0,"wrappedEvent":{"id":"","Name":"","time":"0001-01-01T00:00:00Z","sequenceNo":0},"source":"external-service","typeName":"*Area"}`, time.Time{}).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("123"))
	mock.ExpectCommit()

	store, err := New(context.Background(), db, WithEventTypes(&Area{}, &eventsourced.ExternalEvent[*Area]{}))
	assert.NoError(t, err)

	event := &eventsourced.ExternalEvent[eventsourced.Event]{
		BaseEvent: eventsourced.BaseEvent{
			EventAggregateId: eventsourced.NewEventAggregateId("external"),
		},
		Source:       "external-service",
		TypeName:     "*Area",
		WrappedEvent: &Area{},
	}

	err = store.StoreEvent(
		context.Background(),
		&Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}},
		event,
		1,
		false,
	)
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreEvent(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectBegin()
	mock.ExpectQuery("insert into events (name, aggregate_id, aggregate_name, aggregate_sequence_no, payload, tstamp) values ($1, $2, $3, $4, $5, $6) returning global_sequence_no").
		WithArgs("pg.Area", "123", "pg.Asset", 1, `{"id":"123","Name":"Test","time":"0001-01-01T00:00:00Z","sequenceNo":0}`, tstamp).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("123"))
	mock.ExpectCommit()

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	area := &Area{
		EventAggregateId: eventsourced.NewEventAggregateId("123"),
		Name:             "Test",
	}

	err = store.StoreEvent(
		context.Background(),
		&Asset{BaseAggregate: BaseAggregate{ID: eventsourced.
			Id("123")}},
		area,
		1,
		false,
	)
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreEvent_UnregisteredType(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	store, err := New(context.Background(), db)
	assert.NoError(t, err)

	area := &Area{
		EventAggregateId: eventsourced.NewEventAggregateId("123"),
		Name:             "Test",
	}

	err = store.StoreEvent(
		context.Background(),
		&Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}},
		area,
		1,
		false,
	)
	assert.EqualError(t, err, "no event type <pg.Area> found in registered event types []")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreEvent_NoTypes(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	_, err = New(context.Background(), db)
	assert.NoError(t, err)
	// Enable when empty types are deprecated
	//assert.EqualError(t, err, "empty type mapping in eventstore")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreEvent_ErrorStartingTransaction(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectBegin().WillReturnError(errors.New("error starting transaction"))

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	area := &Area{
		EventAggregateId: eventsourced.NewEventAggregateId("123"),
		Name:             "Test",
	}

	err = store.StoreEvent(
		context.Background(),
		&Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}},
		area,
		1,
		false,
	)
	assert.EqualError(t, err, "error starting transaction")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreEvent_ErrorCreatingAggregate(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectBegin()
	mock.ExpectExec("insert into aggregates (id, name) values ($1, $2)").
		WithArgs("123", "pg.Asset").
		WillReturnError(errors.New("error running query"))

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	area := &Area{
		EventAggregateId: eventsourced.NewEventAggregateId("123"),
		Name:             "Test",
	}

	err = store.StoreEvent(
		context.Background(),
		&Asset{BaseAggregate: BaseAggregate{}},
		area,
		1,
		true,
	)
	assert.EqualError(t, err, "error running query")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreEvent_AggregateAlreadyExists(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectBegin()
	mock.ExpectExec("insert into aggregates (id, name) values ($1, $2)").
		WithArgs("123", "pg.Asset").
		WillReturnError(&pq.Error{Code: integrityConstraintViolation})

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	area := &Area{
		EventAggregateId: eventsourced.NewEventAggregateId("123"),
		Name:             "Test",
	}

	err = store.StoreEvent(
		context.Background(),
		&Asset{BaseAggregate: BaseAggregate{}},
		area,
		1,
		true,
	)
	assert.EqualError(t, err, "aggregate with provided id and type already exists")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreEvent_ErrorDuringCommit(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectBegin()
	mock.ExpectQuery("insert into events (name, aggregate_id, aggregate_name, aggregate_sequence_no, payload, tstamp) values ($1, $2, $3, $4, $5, $6) returning global_sequence_no").
		WithArgs("pg.Area", "123", "pg.Asset", 1, `{"id":"123","Name":"Test","time":"0001-01-01T00:00:00Z","sequenceNo":0}`, tstamp).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("123"))
	mock.ExpectCommit().WillReturnError(errors.New("error during commit"))

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	area := &Area{
		EventAggregateId: eventsourced.NewEventAggregateId("123"),
		Name:             "Test",
	}

	err = store.StoreEvent(
		context.Background(),
		&Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}},
		area,
		1,
		false,
	)
	assert.EqualError(t, err, "error during commit")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreEvent_ErrorMarshal(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	unmarshallableEvent := UnmarshallableEvent(math.Inf(1))
	store, err := New(context.Background(), db, WithEventTypes(&Area{}, &unmarshallableEvent))
	assert.NoError(t, err)

	event := UnmarshallableEvent(math.Inf(1))
	err = store.StoreEvent(
		context.Background(),
		&Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}},
		&event,
		1,
		false,
	)
	assert.EqualError(t, err, "json: unsupported value: +Inf")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreEvent_StaleAggregate(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectBegin()
	mock.ExpectQuery("insert into events (name, aggregate_id, aggregate_name, aggregate_sequence_no, payload, tstamp) values ($1, $2, $3, $4, $5, $6) returning global_sequence_no").
		WithArgs("pg.Area", "123", "pg.Asset", 1, `{"id":"123","Name":"Test","time":"0001-01-01T00:00:00Z","sequenceNo":0}`, tstamp).
		WillReturnError(&pq.Error{Code: integrityConstraintViolation})

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	area := &Area{
		EventAggregateId: eventsourced.NewEventAggregateId("123"),
		Name:             "Test",
	}
	err = store.StoreEvent(
		context.Background(),
		&Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}},
		area,
		1,
		false,
	)
	assert.EqualError(t, err, "the aggregate provided has been updated by someone else")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreEvent_ErrorStoring(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectBegin()
	mock.ExpectQuery("insert into events (name, aggregate_id, aggregate_name, aggregate_sequence_no, payload, tstamp) values ($1, $2, $3, $4, $5, $6) returning global_sequence_no").
		WithArgs("pg.Area", "123", "pg.Asset", 1, `{"id":"123","Name":"Test","time":"0001-01-01T00:00:00Z","sequenceNo":0}`, tstamp).
		WillReturnError(errors.New("attribute name does not exist in table events"))

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	area := &Area{
		EventAggregateId: eventsourced.NewEventAggregateId("123"),
		Name:             "Test",
	}
	err = store.StoreEvent(
		context.Background(),
		&Asset{BaseAggregate: BaseAggregate{ID: eventsourced.Id("123")}},
		area,
		1,
		false,
	)
	assert.EqualError(t, err, "attribute name does not exist in table events")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreSnapshot(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectExec("insert into snapshots (name, aggregate_id, sequence_no, payload, tstamp) values ($1, $2, $3, $4, $5)").
		WithArgs("pg.TestAggregate", "123", 3, `{"ID":"123","Name":"Test"}`, sqlmock.AnyArg()).
		WillReturnResult(sqlmock.NewResult(0, 1))

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	err = store.StoreSnapshot(context.Background(), &TestAggregate{ID: "123", Name: "Test"}, 3)
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreSnapshot_NonPointer(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	err = store.StoreSnapshot(context.Background(), TestAggregate{ID: "123", Name: "Test"}, 3)
	assert.EqualError(t, err, "StoreSnapshot requires a pointer to an aggregate")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_StoreSnapshot_ErrorMarshal(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	store, err := New(context.Background(), db, WithEventTypes(&Area{}))
	assert.NoError(t, err)

	aggregate := UnmarshallableAggregate(math.Inf(1))
	err = store.StoreSnapshot(context.Background(), &aggregate, 3)
	assert.EqualError(t, err, "json: unsupported value: +Inf")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_RestoreSnapshot_QueryError(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectQuery("select id, name, aggregate_id, sequence_no, payload, tstamp from snapshots where aggregate_id = $1 and name = $2 order by sequence_no DESC limit 1").
		WithArgs("123", "pg.TestAggregate").
		WillReturnError(errors.New("error fetching snapshot"))

	store, err := New(context.Background(), db, WithEventTypes(&AssetCreated{}))
	assert.NoError(t, err)

	aggregate := &TestAggregate{ID: "123"}
	_, err = store.RestoreSnapshot(context.Background(), aggregate)
	assert.EqualError(t, err, "error fetching snapshot")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_RestoreSnapshot_NotFound(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectQuery("select id, name, aggregate_id, sequence_no, payload, tstamp from snapshots where aggregate_id = $1 and name = $2 order by sequence_no DESC limit 1").
		WithArgs("123", "pg.TestAggregate").
		WillReturnRows(
			mock.NewRows([]string{"id", "name", "aggregate_id", "sequence_no", "payload", "tstamp"}))

	store, err := New(context.Background(), db, WithEventTypes(&AssetRemoved{}))
	assert.NoError(t, err)

	aggregate := &TestAggregate{ID: "123"}
	snapshot, err := store.RestoreSnapshot(context.Background(), aggregate)
	assert.NoError(t, err)
	assert.Nil(t, snapshot)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_RestoreSnapshot_ScanError(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectQuery("select id, name, aggregate_id, sequence_no, payload, tstamp from snapshots where aggregate_id = $1 and name = $2 order by sequence_no DESC limit 1").
		WithArgs("123", "pg.TestAggregate").
		WillReturnRows(
			mock.NewRows([]string{"id", "name", "aggregate_id", "sequence_no", "payload", "tstamp"}).
				AddRow("abc", "def", "123", 1, "{}", tstamp))

	store, err := New(context.Background(), db, WithEventTypes(&AssetRemoved{}))
	assert.NoError(t, err)

	aggregate := &TestAggregate{ID: "123"}
	_, err = store.RestoreSnapshot(context.Background(), aggregate)
	assert.EqualError(t, err, "sql: Scan error on column index 0, name \"id\": converting driver.Value type string (\"abc\") to a int64: invalid syntax")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_RestoreSnapshot_UnmarshalError(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectQuery("select id, name, aggregate_id, sequence_no, payload, tstamp from snapshots where aggregate_id = $1 and name = $2 order by sequence_no DESC limit 1").
		WithArgs("123", "pg.TestAggregate").
		WillReturnRows(
			mock.NewRows([]string{"id", "name", "aggregate_id", "sequence_no", "payload", "tstamp"}).
				AddRow(1, "def", "123", 1, []byte(`{`), tstamp))

	store, err := New(context.Background(), db, WithEventTypes(&AssetRemoved{}))
	assert.NoError(t, err)

	aggregate := &TestAggregate{ID: "123"}
	_, err = store.RestoreSnapshot(context.Background(), aggregate)
	assert.EqualError(t, err, "unexpected end of JSON input")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_RestoreSnapshot_NonPointer(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	store, err := New(context.Background(), db, WithEventTypes(&AssetRemoved{}))
	assert.NoError(t, err)

	aggregate := TestAggregate{ID: "123"}
	_, err = store.RestoreSnapshot(context.Background(), aggregate)
	assert.EqualError(t, err, "RestoreSnapshot requires a pointer to an aggregate")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_RestoreSnapshot(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectQuery("select id, name, aggregate_id, sequence_no, payload, tstamp from snapshots where aggregate_id = $1 and name = $2 order by sequence_no DESC limit 1").
		WithArgs("123", "pg.TestAggregate").
		WillReturnRows(
			mock.NewRows([]string{"id", "name", "aggregate_id", "sequence_no", "payload", "tstamp"}).
				AddRow(1, "def", "123", 1, []byte(`{"id":"abc", "name":"def"}`), tstamp))

	store, err := New(context.Background(), db, WithEventTypes(&AssetRemoved{}))
	assert.NoError(t, err)

	aggregate := &TestAggregate{ID: "123"}
	snap, err := store.RestoreSnapshot(context.Background(), aggregate)
	assert.NoError(t, err)
	assert.Equal(t, 1, snap.SequenceNo())
	assert.Equal(t, &TestAggregate{
		ID:   "abc",
		Name: "def",
	}, aggregate)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_GetAggregateRoots(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectQuery("select id from aggregates where name = $1").
		WithArgs(reflect.TypeOf(TestAggregate{}).String()).
		WillReturnRows(
			mock.NewRows([]string{"id"}).
				AddRow("1").
				AddRow("2"))

	store, err := New(context.Background(), db, WithEventTypes(&AssetRemoved{}))
	assert.NoError(t, err)

	roots, err := store.GetAggregateRoots(context.Background(), reflect.TypeOf(TestAggregate{}))
	assert.NoError(t, err)
	assert.Equal(t, 2, len(roots))
	assert.Equal(t, []eventsourced.ID{"1", "2"}, roots)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_GetAggregateRoots_PointerValue(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectQuery("select id from aggregates where name = $1").
		WithArgs(reflect.TypeOf(TestAggregate{}).String()).
		WillReturnRows(
			mock.NewRows([]string{"id"}).
				AddRow("1").
				AddRow("2"))

	store, err := New(context.Background(), db, WithEventTypes(&AssetRemoved{}))
	assert.NoError(t, err)

	roots, err := store.GetAggregateRoots(context.Background(), reflect.TypeOf(&TestAggregate{}))
	assert.NoError(t, err)
	assert.Equal(t, 2, len(roots))
	assert.Equal(t, []eventsourced.ID{"1", "2"}, roots)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStore_GetAggregateRootsError(t *testing.T) {
	db, mock, err := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
	assert.NoError(t, err)

	MigrationExpectations(mock)

	mock.ExpectQuery("select id from aggregates where name = $1").
		WithArgs(reflect.TypeOf(TestAggregate{}).String()).
		WillReturnError(errors.New("error fetching aggregates"))

	store, err := New(context.Background(), db, WithEventTypes(&AssetRemoved{}))
	assert.NoError(t, err)

	_, err = store.GetAggregateRoots(context.Background(), reflect.TypeOf(TestAggregate{}))
	assert.EqualError(t, err, "error fetching aggregates")
	assert.NoError(t, mock.ExpectationsWereMet())
}

func Test_store_Events(t *testing.T) {
	litter.Config.HidePrivateFields = false
	type args struct {
		fromID     int
		eventTypes []eventsourced.Aggregate
	}
	tests := []struct {
		name    string
		setup   func(sqlmock2 sqlmock.Sqlmock)
		args    args
		wantErr bool
		want    []eventsourced.Event
	}{
		{
			name: "error querying without types",
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where global_sequence_no > $1 order by global_sequence_no limit 10000").
					WithArgs(0).
					WillReturnError(errors.New("error"))
			},
			args:    args{},
			wantErr: true,
		},
		{
			name:    "error non pointer types",
			args:    args{eventTypes: []eventsourced.Aggregate{TestAggregate{}}},
			wantErr: true,
		},
		{
			name:    "error querying with types",
			args:    args{eventTypes: []eventsourced.Aggregate{&TestAggregate{}}},
			wantErr: true,
		},
		{
			name: "error scanning result",
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where global_sequence_no > $1 order by global_sequence_no limit 10000").
					WithArgs(0).
					WillReturnRows(
						sqlmock.NewRows([]string{"id", "sequence_no", "name", "payload", "tstamp"}).
							AddRow("1", "1", nil, "payload", time.Time{}),
					)
			},
			args:    args{},
			wantErr: true,
		},
		{
			name: "invalid event type",
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where global_sequence_no > $1 order by global_sequence_no limit 10000").
					WithArgs(0).
					WillReturnRows(
						sqlmock.NewRows([]string{"id", "sequence_no", "name", "payload", "tstamp"}).
							AddRow("1", "1", "SomeEvent", "payload", time.Time{}),
					)
			},
			wantErr: true,
		},
		{
			name: "JSON unmarshal error",
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where global_sequence_no > $1 order by global_sequence_no limit 10000").
					WithArgs(0).
					WillReturnRows(
						sqlmock.NewRows([]string{"id", "sequence_no", "name", "payload", "tstamp"}).
							AddRow("1", "1", "AssetCreated", "{", time.Time{}),
					)
			},
			args:    args{},
			wantErr: true,
		},
		{
			name: "success",
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where global_sequence_no > $1 order by global_sequence_no limit 10000").
					WithArgs(0).
					WillReturnRows(
						sqlmock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
							AddRow("1", "1", "AssetCreated", []byte(`{"id":"abc123", "time": "2020-11-25T13:07:12.000Z", "name": "The asset"}`), time.Time{}),
					)
				mock.
					ExpectQuery("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where global_sequence_no > $1 order by global_sequence_no limit 10000").
					WithArgs(1).
					WillReturnRows(sqlmock.NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}))
			},
			args:    args{},
			wantErr: false,
			want: []eventsourced.Event{
				&AssetCreated{
					EventAggregateId:    eventsourced.NewEventAggregateId("abc123"),
					EventTime:           eventsourced.EventTime{Time: time.Date(2020, 11, 25, 13, 7, 12, 0, time.UTC)},
					EventGlobalSequence: eventsourced.EventGlobalSequence{SeqNo: 1},
					Name:                "The asset",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			if tt.setup != nil {
				tt.setup(mock)
			}
			p := Store{
				db:           sqlx.NewDb(db, "postgres"),
				traceHandler: noOpTraceHandler,
				types: map[string]reflect.Type{
					"AssetCreated": reflect.TypeOf(&AssetCreated{}),
				},
			}
			wg := sync.WaitGroup{}
			wg.Add(1)
			var events []eventsourced.Event
			c := make(chan eventsourced.Event)
			go func() {
				defer wg.Done()
				for e := range c {
					events = append(events, e)
				}
			}()
			if err := p.Events(context.Background(), tt.args.fromID, c, tt.args.eventTypes...); (err != nil) != tt.wantErr {
				t.Errorf("Events() error = %v, wantErr %v", err, tt.wantErr)
			}
			wg.Wait()
			if !reflect.DeepEqual(events, tt.want) {
				t.Errorf("Events() got %s, want %s", litter.Sdump(events), litter.Sdump(tt.want))
			}
			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}
}

func TestStore_MaxGlobalSequenceNo(t *testing.T) {
	type fields struct {
		types               map[string]reflect.Type
		skipUndefinedEvents bool
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		setup   func(mock sqlmock.Sqlmock)
		want    int
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "query error",
			args: args{
				ctx: context.Background(),
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectQuery("with cte as (select global_sequence_no, aggregate_sequence_no, name, payload, tstamp, row_number() over (order by global_sequence_no desc) as rn from events) select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from cte where rn = 1").
					WillReturnError(fmt.Errorf("query error"))
			},
			want: -1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "query error")
			},
		},
		{
			name: "no rows",
			args: args{
				ctx: context.Background(),
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectQuery("with cte as (select global_sequence_no, aggregate_sequence_no, name, payload, tstamp, row_number() over (order by global_sequence_no desc) as rn from events) select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from cte where rn = 1").
					WillReturnError(sql.ErrNoRows)
			},
			want:    0,
			wantErr: assert.NoError,
		},
		{
			name: "success",
			args: args{
				ctx: context.Background(),
			},
			setup: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectQuery("with cte as (select global_sequence_no, aggregate_sequence_no, name, payload, tstamp, row_number() over (order by global_sequence_no desc) as rn from events) select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from cte where rn = 1").
					WillReturnRows(
						sqlmock.
							NewRows([]string{"global_sequence_no", "aggregate_sequence_no", "name", "payload", "tstamp"}).
							AddRow(123, 1, "pg.AssetCreated", []byte(`{}`), time.Time{}),
					)
			},
			want:    123,
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			if tt.setup != nil {
				tt.setup(mock)
			}
			p := Store{
				db:                  sqlx.NewDb(db, "postgres"),
				traceHandler:        noOpTraceHandler,
				types:               tt.fields.types,
				skipUndefinedEvents: tt.fields.skipUndefinedEvents,
			}
			got, err := p.MaxGlobalSequenceNo(tt.args.ctx)
			if !tt.wantErr(t, err, fmt.Sprintf("MaxGlobalSequenceNo(%v)", tt.args.ctx)) {
				return
			}
			assert.Equalf(t, tt.want, got, "MaxGlobalSequenceNo(%v)", tt.args.ctx)
			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}
}

func Test_gooseLoggerWrapper_Fatalf(t *testing.T) {
	type args struct {
		format string
		v      []interface{}
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "no vars",
			args: args{
				format: "abc",
			},
			want: "level=ERROR msg=abc part=pg\n",
		},
		{
			name: "with vars",
			args: args{
				format: "abc %s",
				v:      []interface{}{"xxx"},
			},
			want: "level=ERROR msg=\"abc xxx\" part=pg\n",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			buff := &bytes.Buffer{}
			logger := slog.New(slog.NewTextHandler(buff, &slog.HandlerOptions{ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
				if a.Key == "time" {
					return slog.Attr{}
				}
				return a
			}}))
			g := gooseLoggerWrapper{
				logger: logger,
			}
			g.Fatalf(tt.args.format, tt.args.v...)
			assert.Equal(t, tt.want, buff.String())
		})
	}
}

func Test_gooseLoggerWrapper_Printf(t *testing.T) {
	type args struct {
		format string
		v      []interface{}
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "no vars",
			args: args{
				format: "abc",
			},
			want: "level=INFO msg=abc part=pg\n",
		},
		{
			name: "with vars",
			args: args{
				format: "abc %s",
				v:      []interface{}{"xxx"},
			},
			want: "level=INFO msg=\"abc xxx\" part=pg\n",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			buff := &bytes.Buffer{}
			logger := slog.New(slog.NewTextHandler(buff, &slog.HandlerOptions{ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
				if a.Key == "time" {
					return slog.Attr{}
				}
				return a
			}}))
			g := gooseLoggerWrapper{
				logger: logger,
			}
			g.Printf(tt.args.format, tt.args.v...)
			assert.Equal(t, tt.want, buff.String())
		})
	}
}

type TestAggregate struct {
	ID   string
	Name string
}

func (t TestAggregate) SetIdentity(id eventsourced.ID) {
}

func (t TestAggregate) Identity() *eventsourced.ID {
	id := eventsourced.ID(t.ID)
	return &id
}

func (t TestAggregate) Apply(_ eventsourced.Event) error {
	return nil
}

var _ eventsourced.Aggregate = &TestAggregate{}

type Area struct {
	eventsourced.EventAggregateId
	Name string
	eventsourced.EventTime
	eventsourced.EventGlobalSequence
}

func (a *Area) When() time.Time {
	return tstamp
}

func (a *Area) SetWhen(_ time.Time) {
}

var _ eventsourced.Event = &Area{}

type UnmarshallableEvent float64

func (e UnmarshallableEvent) SetAggregateIdentity(_ eventsourced.ID) {
}

func (UnmarshallableEvent) AggregateIdentity() eventsourced.ID {
	return ""
}

func (UnmarshallableEvent) When() time.Time {
	return tstamp
}

func (UnmarshallableEvent) SetWhen(_ time.Time) {
}

func (e UnmarshallableEvent) GlobalSequenceNo() int {
	panic("implement me")
}

func (e UnmarshallableEvent) SetGlobalSequenceNo(seqNo int) {
	panic("implement me")
}

var _ eventsourced.Event = UnmarshallableEvent(1.0)

type UnmarshallableAggregate float64

func (u UnmarshallableAggregate) SetIdentity(id eventsourced.ID) {
}

func (u UnmarshallableAggregate) Identity() *eventsourced.ID {
	id := eventsourced.ID("")
	return &id
}

func (u UnmarshallableAggregate) Apply(_ eventsourced.Event) error {
	return nil
}

var _ eventsourced.Aggregate = UnmarshallableAggregate(1.0)

type BaseAggregate struct {
	ID *eventsourced.ID
}

func (b BaseAggregate) Identity() *eventsourced.ID {
	return b.ID
}

type Asset struct {
	BaseAggregate
	Name    string
	Created time.Time
	Updated time.Time
	Deleted bool
}

func (a *Asset) SetIdentity(id eventsourced.ID) {
	a.ID = &id
}

func (a *Asset) Apply(event eventsourced.Event) error {
	switch e := event.(type) {
	case *AssetCreated:
		id := event.AggregateIdentity()
		a.ID = &id
		a.Name = e.Name
		a.Updated = e.When()
		a.Created = e.When()
	case *AssetRemoved:
		a.Deleted = true
		a.Updated = e.When()
	}
	return nil
}

var _ eventsourced.Aggregate = &Asset{}

type AssetNonPointer struct {
	BaseAggregate
	Name    string
	Created time.Time
	Updated time.Time
	Deleted bool
}

func (a AssetNonPointer) SetIdentity(id eventsourced.ID) {
}

func (a AssetNonPointer) Apply(event eventsourced.Event) error {
	return nil
}

var _ eventsourced.Aggregate = &AssetNonPointer{}

type AssetRemoved struct {
	eventsourced.EventAggregateId
	eventsourced.EventTime
	eventsourced.EventGlobalSequence
}

var _ eventsourced.Event = &AssetRemoved{}

type AssetCreated struct {
	eventsourced.EventAggregateId
	eventsourced.EventTime
	eventsourced.EventGlobalSequence
	Name string `json:"name"`
}

var _ eventsourced.Event = &AssetCreated{}

type CreateAsset struct {
	Name string
}

func (c CreateAsset) Validate(_ context.Context, aggregate eventsourced.Aggregate) error {
	if aggregate.Identity() != nil {
		return fmt.Errorf("asset already exists (id: '%s'", aggregate.Identity())
	}
	return nil
}

func (c CreateAsset) Event(_ context.Context) eventsourced.Event {
	return &AssetCreated{
		Name: c.Name,
	}
}

var _ eventsourced.Command = &CreateAsset{}

type NonPointerEvent string

func (n NonPointerEvent) AggregateIdentity() eventsourced.ID {
	return ""
}

func (n NonPointerEvent) SetAggregateIdentity(eventsourced.ID) {
}

func (n NonPointerEvent) When() time.Time {
	return time.Time{}
}

func (n NonPointerEvent) SetWhen(time.Time) {
}

func (n NonPointerEvent) GlobalSequenceNo() int {
	panic("implement me")
}

func (n NonPointerEvent) SetGlobalSequenceNo(seqNo int) {
	panic("implement me")
}

var _ eventsourced.Event = NonPointerEvent("")

type ExternalEvent struct {
}

func (e ExternalEvent) AggregateIdentity() eventsourced.ID {
	return "external-id"
}

func (e ExternalEvent) SetAggregateIdentity(id eventsourced.ID) {
}

func (e ExternalEvent) When() time.Time {
	return time.Time{}
}

func (e ExternalEvent) SetWhen(t time.Time) {
}

func (e ExternalEvent) GlobalSequenceNo() int {
	panic("implement me")
}

func (e ExternalEvent) SetGlobalSequenceNo(seqNo int) {
	panic("implement me")
}

func (e ExternalEvent) ExternalSource() string {
	return "external"
}

var _ eventsourced.Event = &ExternalEvent{}
var _ eventsourced.ExternalSource = &ExternalEvent{}

func MigrationExpectations(mock sqlmock.Sqlmock) {
	mock.ExpectQuery("SELECT EXISTS ( SELECT 1 FROM pg_tables WHERE (current_schema() IS NULL OR schemaname = current_schema()) AND tablename = 'goose_db_version_pg' )").
		WithArgs().WillReturnRows(sqlmock.NewRows([]string{""}).AddRow("true"))
	mock.ExpectQuery("SELECT version_id, is_applied from goose_db_version_pg ORDER BY id DESC").
		WillReturnRows(sqlmock.
			NewRows([]string{"tstamp", "is_applied"}).
			AddRow(20230928153900, true).
			AddRow(20231017133000, true).
			AddRow(20231123082300, true).
			AddRow(20240314110100, true),
		)
}
