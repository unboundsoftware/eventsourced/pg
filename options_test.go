/*
 * MIT License
 *
 * Copyright (c) 2023 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package pg

import (
	"context"
	"log/slog"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/unboundsoftware/eventsourced/eventsourced"
)

func TestWithTraceHandler(t *testing.T) {
	store := &Store{}
	err := WithTraceHandler(nil)(store)
	assert.EqualError(t, err, "traceHandler must be set to a non-nil value")
	tracer := &MockTracer{}
	err = WithTraceHandler(tracer)(store)
	assert.NoError(t, err)
	assert.Equal(t, tracer, store.traceHandler)
}

func TestWithLogger(t *testing.T) {
	store := &Store{}
	err := WithLogger(nil)(store)
	assert.EqualError(t, err, "logger must be set to a non-nil value")
	logger := slog.Default()
	err = WithLogger(logger)(store)
	assert.NoError(t, err)
	assert.Equal(t, logger, store.logger)
}

func TestWithEventTypes(t *testing.T) {
	type args struct {
		eventTypes []interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "non pointer event",
			args: args{eventTypes: []interface{}{NonPointerEvent("")}},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "event type pg.NonPointerEvent must be pointer")
			},
		},
		{
			name: "duplicated JSON field names",
			args: args{eventTypes: []interface{}{&DuplicatedNames{}}},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, `json field 'id' defined on field 'ID' is already used by field 'Inner.Duplicated' on type 'DuplicatedNames'
json field 'id' defined on field 'Named.Inner.Duplicated' is already used by field 'Named.ID' on type 'DuplicatedNames'
json field 'id' defined on field 'Pointer.Inner.Duplicated' is already used by field 'Pointer.ID' on type 'DuplicatedNames'
json field 'Named' defined on field 'Other' is already used by field 'Named' on type 'DuplicatedNames'
json field 'id' defined on field 'Other.Inner.Duplicated' is already used by field 'Other.ID' on type 'DuplicatedNames'`)
			},
		},
		{
			name:    "success",
			args:    args{eventTypes: []interface{}{&AssetCreated{}}},
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			store := &Store{}
			tt.wantErr(t, WithEventTypes(tt.args.eventTypes...)(store))
		})
	}
}

type MockTracer struct{}

func (m *MockTracer) Trace(ctx context.Context, name string) (context.Context, func()) {
	panic("implement me")
}

var _ eventsourced.TraceHandler = &MockTracer{}

type Inner struct {
	Duplicated string `json:"id"`
}

type Named struct {
	ID string `json:"id"`
	Inner
}

type DuplicatedNames struct {
	Inner
	ID      string `json:"id"`
	id      string
	Named   Named
	Pointer *Named `json:"pointer"`
	Other   Named  `json:"Named"`
}

func (d DuplicatedNames) AggregateIdentity() eventsourced.ID {
	panic("implement me")
}

func (d DuplicatedNames) SetAggregateIdentity(id eventsourced.ID) {
	panic("implement me")
}

func (d DuplicatedNames) When() time.Time {
	panic("implement me")
}

func (d DuplicatedNames) SetWhen(t time.Time) {
	panic("implement me")
}

func (d DuplicatedNames) GlobalSequenceNo() int {
	panic("implement me")
}

func (d DuplicatedNames) SetGlobalSequenceNo(seqNo int) {
	panic("implement me")
}

func (d DuplicatedNames) String() string {
	return d.id
}

var _ eventsourced.Event = &DuplicatedNames{}
