/*
 * MIT License
 *
 * Copyright (c) 2024 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package pg

import (
	"database/sql/driver"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPayload_Scan(t *testing.T) {
	type args struct {
		value interface{}
	}
	tests := []struct {
		name    string
		args    args
		want    Payload
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "unexpected type",
			args: args{value: "abc"},
			want: Payload(nil),
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "type assertion to []byte failed")
			},
		},
		{
			name:    "success",
			args:    args{value: []byte(`{"name":"abc"}`)},
			want:    Payload(`{"name":"abc"}`),
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var p Payload
			tt.wantErr(t, p.Scan(tt.args.value), fmt.Sprintf("Scan(%v)", tt.args.value))
			assert.Equal(t, tt.want, p)
		})
	}
}

func TestPayload_Value(t *testing.T) {
	tests := []struct {
		name    string
		p       Payload
		want    driver.Value
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name:    "success",
			p:       Payload(`{"name":"abc"}`),
			want:    []byte(`{"name":"abc"}`),
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.p.Value()
			if !tt.wantErr(t, err, "Value()") {
				return
			}
			assert.Equalf(t, tt.want, got, "Value()")
		})
	}
}
