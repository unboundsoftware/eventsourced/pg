// Copyright (c) 2020 Unbound Software Development Svenska AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package pg

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"reflect"
	"regexp"
	"slices"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"github.com/pressly/goose/v3"
	"github.com/pressly/goose/v3/database"

	"gitlab.com/unboundsoftware/eventsourced/eventsourced"

	"gitlab.com/unboundsoftware/eventsourced/pg/migrations"
)

const integrityConstraintViolation = "23505"

type Store struct {
	db                  *sqlx.DB
	types               map[string]reflect.Type
	skipUndefinedEvents bool
	traceHandler        eventsourced.TraceHandler
	logger              Logger
}

func (p Store) GetAggregateRoots(ctx context.Context, aggregateType reflect.Type) ([]eventsourced.ID, error) {
	ctx, after := p.traceHandler.Trace(ctx, "pg.GetAggregateRoots")
	defer after()
	var aggregates []eventsourced.ID

	s := aggregateType.String()
	if aggregateType.Kind() == reflect.Ptr {
		s = aggregateType.Elem().String()
	}
	if err := p.db.SelectContext(ctx, &aggregates, "select id from aggregates where name = $1", s); err != nil {
		return nil, err
	}
	return aggregates, nil
}

// Event is the struct matching the events-table in the database
type event struct {
	GlobalSequenceNo int `db:"global_sequence_no"`
	SequenceNo       int `db:"aggregate_sequence_no"`
	Name             string
	Payload          Payload
	Tstamp           time.Time
}

func (p Store) GetEvents(ctx context.Context, aggregate eventsourced.Aggregate, sinceSnapshot eventsourced.Snapshot) ([]eventsourced.EventWrapper, error) {
	ctx, after := p.traceHandler.Trace(ctx, "pg.GetEvents")
	defer after()
	aggregateName, err := typeName(aggregate)
	if err != nil {
		return nil, errors.New("GetEvents requires a pointer to an aggregate")
	}
	aggregateSeqNo := 0
	if sinceSnapshot != nil {
		aggregateSeqNo = sinceSnapshot.SequenceNo()
	}
	var events []event
	err = p.db.SelectContext(ctx, &events, `select global_sequence_no, aggregate_sequence_no, name, payload, tstamp
	from events
	where aggregate_id = $1 and aggregate_name = $2 and aggregate_sequence_no > $3 order by aggregate_sequence_no`,
		aggregate.Identity(), aggregateName, aggregateSeqNo)
	if err != nil {
		return nil, err
	}
	var res []eventsourced.EventWrapper
	for _, e := range events {
		wrapper, err := p.toWrapper(e)
		if err != nil {
			return nil, err
		}
		if wrapper != nil {
			res = append(res, *wrapper)
		}
	}
	return res, nil
}

func (p Store) Events(ctx context.Context, fromGlobalSequenceNo int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error {
	ctx, after := p.traceHandler.Trace(ctx, "pg.Events")
	defer after()
	defer close(c)
	var rows *sqlx.Rows
	var err error

	for {
		if len(aggregateTypes) == 0 {
			rows, err = p.db.QueryxContext(ctx, "select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where global_sequence_no > $1 order by global_sequence_no limit 10000", fromGlobalSequenceNo)
		} else {
			var names []string
			for _, a := range aggregateTypes {
				name, err := typeName(a)
				if err != nil {
					return err
				}
				names = append(names, name)
			}
			arg := map[string]interface{}{
				"fromGlobalSequenceNo": fromGlobalSequenceNo,
				"names":                names,
			}
			var query string
			var args []interface{}
			// Ignore error since it can't happen
			query, args, _ = sqlx.Named("select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from events where global_sequence_no > :fromGlobalSequenceNo and aggregate_name in (:names) order by global_sequence_no limit 10000", arg)
			// Ignore error since it can't happen
			query, args, _ = sqlx.In(query, args...)
			query = p.db.Rebind(query)
			rows, err = p.db.QueryxContext(ctx, query, args...)
		}
		if err != nil {
			return err
		}
		rowsFound := false
		for rows.Next() {
			rowsFound = true
			var e event
			err := rows.StructScan(&e)
			if err != nil {
				return err
			}
			fromGlobalSequenceNo = e.GlobalSequenceNo
			wrapper, err := p.toWrapper(e)
			if err != nil {
				return err
			}
			if wrapper == nil {
				continue
			}
			c <- wrapper.Event
		}
		if !rowsFound {
			break
		}
	}

	return nil
}

func (p Store) toWrapper(e event) (*eventsourced.EventWrapper, error) {
	t, err := p.eventType(e.Name)
	if err != nil {
		if p.skipUndefinedEvents {
			return nil, nil
		}
		return nil, err
	}
	x := reflect.New(t.Elem())

	if err := json.Unmarshal(e.Payload, x.Interface()); err != nil {
		return nil, err
	}

	evt := x.Interface().(eventsourced.Event)
	evt.SetGlobalSequenceNo(e.GlobalSequenceNo)

	wrapper := &eventsourced.EventWrapper{
		Event:               evt,
		AggregateSequenceNo: e.SequenceNo,
	}

	return wrapper, nil
}

func (p Store) MaxGlobalSequenceNo(ctx context.Context) (int, error) {
	ctx, after := p.traceHandler.Trace(ctx, "pg.MaxGlobalSequenceNo")
	defer after()

	var e event
	row := p.db.QueryRowxContext(ctx, "with cte as (select global_sequence_no, aggregate_sequence_no, name, payload, tstamp, row_number() over (order by global_sequence_no desc) as rn from events) select global_sequence_no, aggregate_sequence_no, name, payload, tstamp from cte where rn = 1")
	err := row.StructScan(&e)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return 0, nil
		}
		return -1, err
	}

	return e.GlobalSequenceNo, nil
}

func (p Store) StoreEvent(ctx context.Context, aggregate eventsourced.Aggregate, event eventsourced.Event, aggregateSequenceNo int, newAggregate bool) error {
	ctx, after := p.traceHandler.Trace(ctx, "pg.StoreEvent")
	defer after()
	aggregateName, err := typeName(aggregate)
	if err != nil {
		return errors.New("StoreEvent requires a pointer to an aggregate")
	}
	name, err := typeName(event)
	if err != nil {
		return errors.New("StoreEvent requires a pointer to an event")
	}
	if external, ok := event.(*eventsourced.ExternalEvent[eventsourced.Event]); ok {
		_, err := typeName(external.WrappedEvent)
		if err != nil {
			return errors.New("StoreEvent requires a pointer to wrapped events")
		}
		wrapped := reflect.TypeOf(external.WrappedEvent).Elem()
		pkgPath := wrapped.PkgPath()
		wrappedName := wrapped.Name()
		name = regexp.MustCompile(`\[.*]`).ReplaceAllString(name, fmt.Sprintf("[*%s.%s]", pkgPath, wrappedName))
	}
	if _, err := p.eventType(name); err != nil {
		return err
	}
	bytes, err := json.Marshal(event)
	if err != nil {
		return err
	}
	tx, err := p.db.Begin()
	if err != nil {
		return err
	}
	defer func() { _ = tx.Rollback() }()

	if newAggregate {
		_, err = tx.ExecContext(ctx, "insert into aggregates (id, name) values ($1, $2)", event.AggregateIdentity(), aggregateName)
		if err != nil {
			var pgErr *pq.Error
			if errors.As(err, &pgErr) && pgErr.Code == integrityConstraintViolation {
				return eventsourced.ErrAggregateAlreadyExists
			}
			return err
		}
	}
	var globalSequenceNo int
	row := tx.QueryRowContext(ctx, "insert into events (name, aggregate_id, aggregate_name, aggregate_sequence_no, payload, tstamp) values ($1, $2, $3, $4, $5, $6) returning global_sequence_no", name, event.AggregateIdentity(), aggregateName, aggregateSequenceNo, string(bytes), event.When())
	err = row.Scan(&globalSequenceNo)
	event.SetGlobalSequenceNo(globalSequenceNo)
	if err != nil {
		var pgErr *pq.Error
		if errors.As(err, &pgErr) {
			if pgErr.Code == integrityConstraintViolation {
				return eventsourced.ErrStaleAggregate
			}
		}
		return err
	}
	err = tx.Commit()
	if err != nil {
		return err
	}
	return err
}

func (p Store) eventType(name string) (reflect.Type, error) {
	t := p.types[name]
	if t == nil {
		keys := make([]string, 0, len(p.types))
		for k := range p.types {
			keys = append(keys, k)
		}
		slices.Sort(keys)
		return nil, fmt.Errorf("no event type <%s> found in registered event types %v", name, keys)
	}
	return t, nil
}

func typeName(v interface{}) (string, error) {
	t := reflect.TypeOf(v)
	if t.Kind() != reflect.Ptr {
		return "", errors.New("a pointer is required")
	}
	return t.Elem().String(), nil
}

type snapshotModel struct {
	ID          int64
	Name        string
	AggregateID string `db:"aggregate_id"`
	SequenceNo  int    `db:"sequence_no"`
	Payload     Payload
	Tstamp      time.Time
}

func (p Store) RestoreSnapshot(ctx context.Context, aggregate eventsourced.Aggregate) (eventsourced.Snapshot, error) {
	ctx, after := p.traceHandler.Trace(ctx, "pg.RestoreSnapshot")
	defer after()
	aggregateName, err := typeName(aggregate)
	if err != nil {
		return nil, errors.New("RestoreSnapshot requires a pointer to an aggregate")
	}
	snap := &snapshotModel{}
	rows, err := p.db.QueryxContext(ctx, "select id, name, aggregate_id, sequence_no, payload, tstamp from snapshots where aggregate_id = $1 and name = $2 order by sequence_no DESC limit 1", aggregate.Identity(), aggregateName)
	if err != nil {
		return nil, err
	}
	defer func() { _ = rows.Close() }()

	if !rows.Next() {
		return nil, nil
	}
	err = rows.StructScan(&snap)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(snap.Payload, aggregate); err != nil {
		return nil, err
	}

	return &snapshot{
		sequenceNo: snap.SequenceNo,
		aggregate:  aggregate,
		when:       snap.Tstamp,
	}, nil
}

func (p Store) StoreSnapshot(ctx context.Context, aggregate eventsourced.Aggregate, aggregateSequenceNo int) error {
	ctx, after := p.traceHandler.Trace(ctx, "pg.StoreSnapshot")
	defer after()
	name, err := typeName(aggregate)
	if err != nil {
		return errors.New("StoreSnapshot requires a pointer to an aggregate")
	}
	bytes, err := json.Marshal(aggregate)
	if err != nil {
		return err
	}
	_, err = p.db.ExecContext(ctx, "insert into snapshots (name, aggregate_id, sequence_no, payload, tstamp) values ($1, $2, $3, $4, $5)", name, aggregate.Identity(), aggregateSequenceNo, string(bytes), time.Now())
	return err
}

// New instantiates an event store using the provided SQL-connection (PostgreSQL) and event types
func New(ctx context.Context, db *sql.DB, opts ...Option) (*Store, error) {
	sdb := sqlx.NewDb(db, "postgres")
	s := &Store{
		db:           sdb,
		traceHandler: noOpTraceHandler,
		logger:       slog.Default(),
	}
	for _, opt := range opts {
		if err := opt(s); err != nil {
			return nil, fmt.Errorf("option function <%s> failed, %v", getOptionFuncName(opt), err)
		}
	}
	if len(s.types) == 0 {
		// Remove output and return error when empty types are deprecated
		// return nil, fmt.Errorf("empty type mapping in eventstore")
		fmt.Println("empty type mapping in eventstore is deprecated and will return error in upcoming releases")
	}
	if err := runMigrations(ctx, db, s.logger); err != nil {
		return nil, err
	}
	return s, nil
}

func runMigrations(ctx context.Context, db *sql.DB, logger Logger) error {
	gooseLogger := gooseLoggerWrapper{logger: logger}
	store, err := database.NewStore(database.DialectPostgres, "goose_db_version_pg")
	if err != nil {
		return err
	}
	provider, err := goose.NewProvider(
		"",
		db,
		migrations.FS,
		goose.WithStore(store),
	)
	if err != nil {
		return err
	}
	goose.SetLogger(gooseLogger)
	_, err = provider.Up(ctx)
	return err
}

type gooseLoggerWrapper struct {
	logger Logger
}

func (g gooseLoggerWrapper) Fatalf(format string, v ...interface{}) {
	g.logger.Error(fmt.Sprintf(format, v...), "part", "pg")
}

func (g gooseLoggerWrapper) Printf(format string, v ...interface{}) {
	g.logger.Info(fmt.Sprintf(format, v...), "part", "pg")
}

type snapshot struct {
	sequenceNo int
	aggregate  eventsourced.Aggregate
	when       time.Time
}

func (s *snapshot) SequenceNo() int {
	return s.sequenceNo
}

func (s *snapshot) When() time.Time {
	return s.when
}

var _ eventsourced.Snapshot = &snapshot{}

var noOpTraceHandler eventsourced.TraceHandler = eventsourced.TraceHandlerFunc(func(ctx context.Context, _ string) (context.Context, func()) {
	return ctx, func() {}
})
