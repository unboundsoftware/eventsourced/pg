/*
 * MIT License
 *
 * Copyright (c) 2020 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package pg

import (
	"errors"
	"fmt"
	"reflect"
	"runtime"
	"strings"

	"gitlab.com/unboundsoftware/eventsourced/eventsourced"
)

// WithEventTypes registers event types with the event store
func WithEventTypes(eventTypes ...interface{}) Option {
	return func(store *Store) error {
		x := make(map[string]reflect.Type)
		evtInterface := reflect.TypeOf((*eventsourced.Event)(nil)).Elem()
		errs := make([]error, 0)
		for _, et := range eventTypes {
			t := reflect.TypeOf(et)
			if t.Kind() != reflect.Ptr {
				errs = append(errs, fmt.Errorf("event type %s must be pointer", t.String()))
				x[t.String()] = t
			} else {
				if !t.Implements(evtInterface) {
					errs = append(errs, fmt.Errorf("event type %s does not implement eventsourced.Event", t.Elem().String()))
				}
				if err := checkFieldTags(t.Elem(), t.Elem(), ""); err != nil {
					errs = append(errs, err)
				}
				x[t.Elem().String()] = t
			}
		}
		if len(errs) > 0 {
			return errors.Join(errs...)
		}
		store.types = x
		return nil
	}
}

func checkFieldTags(t, root reflect.Type, path string) error {
	if errs := doCheckFieldTags(t, root, path, make(map[string]string)); len(errs) > 0 {
		return errors.Join(errs...)
	}
	return nil
}

func doCheckFieldTags(t, root reflect.Type, path string, names map[string]string) []error {
	errs := make([]error, 0)
	if t.Kind() == reflect.Struct {
		for i := 0; i < t.NumField(); i++ {
			field := t.Field(i)

			if e := checkField(field, names, path, root); e != nil {
				errs = append(errs, e...)
			}
		}
	}
	return errs
}

func checkField(field reflect.StructField, names map[string]string, path string, root reflect.Type) []error {
	if !field.IsExported() {
		return nil
	}
	t := field.Type
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	errs := make([]error, 0)
	var tagName string
	if tag, ok := field.Tag.Lookup("json"); ok {
		tagName, _, _ = strings.Cut(tag, ",")
	} else {
		tagName = field.Name
	}

	if name, exists := names[tagName]; exists {
		errs = append(errs, fmt.Errorf("json field '%s' defined on field '%s' is already used by field '%s' on type '%s'", tagName, joinPath(path, field.Name), name, root.Name()))
	} else {
		names[tagName] = joinPath(path, field.Name)
	}

	if t.Kind() == reflect.Struct {
		p := joinPath(path, field.Name)
		if field.Anonymous {
			if e := doCheckFieldTags(t, root, p, names); e != nil {
				errs = append(errs, e...)
			}
		} else {
			if err := checkFieldTags(t, root, p); err != nil {
				errs = append(errs, err)
			}
		}
	}
	return errs
}

func joinPath(path, name string) string {
	if path == "" {
		return name
	}
	return fmt.Sprintf("%s.%s", path, name)
}

// WithIgnoreUndefinedEventTypesDuringReplay ignores undefined event types in the event store
func WithIgnoreUndefinedEventTypesDuringReplay() Option {
	return func(store *Store) error {
		store.skipUndefinedEvents = true
		return nil
	}
}

// WithTraceHandler sets the trace handler to use on the store
func WithTraceHandler(traceHandler eventsourced.TraceHandler) Option {
	return func(store *Store) error {
		if traceHandler == nil {
			return eventsourced.ErrNoTraceHandler
		}
		store.traceHandler = traceHandler
		return nil
	}
}

type Logger interface {
	Info(msg string, args ...any)
	Error(msg string, args ...any)
}

var ErrNoLogger = fmt.Errorf("logger must be set to a non-nil value")

func WithLogger(logger Logger) Option {
	return func(store *Store) error {
		if logger == nil {
			return ErrNoLogger
		}
		store.logger = logger
		return nil
	}
}

// Option is used to modify Store
type Option func(store *Store) error

func getOptionFuncName(f Option) string {
	return runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name()
}
