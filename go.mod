module gitlab.com/unboundsoftware/eventsourced/pg

go 1.21.0

toolchain go1.24.1

require (
	github.com/DATA-DOG/go-sqlmock v1.5.2
	github.com/jmoiron/sqlx v1.4.0
	github.com/lib/pq v1.10.9
	github.com/pressly/goose/v3 v3.24.1
	github.com/sanity-io/litter v1.5.8
	github.com/stretchr/testify v1.10.0
	gitlab.com/unboundsoftware/eventsourced/eventsourced v1.17.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/mfridman/interpolate v0.0.2 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sethvargo/go-retry v0.3.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/sync v0.10.0 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

retract v1.13.4
